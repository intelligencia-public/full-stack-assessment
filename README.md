# Full-stack Engineering Assessment

The goal is to:
1. create a back-end infrastructure that stores and exposes specific data provided by the Ontology Lookup Service repository https://www.ebi.ac.uk/ols/index
2. create a web application that retrieves and displays data from the back-end infrastructure 

## Back-end

We need to be able to store and expose the following:
* EFO terms
* EFO term synonyms

You should implement a web API, taking into account the following:

* data should be stored in table(s) of a PostgreSQL database; the table design is up to you
* you're limited to implementing this with any/all of the following: Python, Django, PostgreSQL
* there should be clear instructions on how to setup and run the project in a new development environment
* please provide the solution as a git code repository in your preferred git hosting service

The implementation will be evaluated against:

* code readability and quality
* database design; you should store data in a normalized schema that eliminates data duplication and enforces data integrity

### Bonus
* you should provide a migration script that inserts testing data (preferably by sampling the OLS API: https://www.ebi.ac.uk/ols/api/ontologies/efo/terms). You can use either a dump or insert them into a django migration.
* EFO term ontology (parent-child relationships, provided by parent links)

## Front-end

You should implement a web application that retrieves EFO term data from the implemented web API, and displays them in a paginated data table:

* you are free to use any technology stack you like, preferably: Typescript, React, Ant design
* there should be clear instructions on how to setup and run the application in a new development environment
* please provide the solution as a git code repository in your preferred git hosting service

The implementation will be evaluated against:

* efficiency of implementation, i.e. minimized response times, re-renders, etc
* use of best practices that promote modularity, code re-use, and maintainability
* code readability and quality

### Bonus:
* implement a text input that allows the user to free-text search the dataset
